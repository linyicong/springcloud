package cn.edu.neusoft.provider.controller;

import cn.edu.neusoft.provider.entity.Tables;
import cn.edu.neusoft.provider.entity.tables.pojos.User;
import org.jooq.DSLContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-21 14:01
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {

    @Autowired
    private DSLContext create;

    /**
     * 测试：JOOQ根据id查询User表的数据
     * 测试结果：User (1, account1, 张三, 20, 100.00)
     */
    @Test
    public void fetchById() {
        User user = create.selectFrom(Tables.USER)
                .where(Tables.USER.ID.eq(1L))
                .fetchOneInto(User.class);
        System.out.println(user);
    }
}