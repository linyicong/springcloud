package cn.edu.neusoft.provider.controller;


import cn.edu.neusoft.provider.entity.Tables;
import cn.edu.neusoft.provider.entity.tables.User;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-21 11:29
 */
@RestController
public class UserController {

    @Autowired
    private DSLContext create;

    @GetMapping("/{id}")
    public User fetchById(@PathVariable Long id) {
        return create.selectFrom(Tables.USER)
                .where(Tables.USER.ID.eq(id))
                .fetchOneInto(User.class);
    }
}
