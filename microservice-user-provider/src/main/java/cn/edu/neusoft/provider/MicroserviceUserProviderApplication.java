package cn.edu.neusoft.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 注解@EnableEurekaClient声明这是一个Eureka Client（仅适用Eureka）
 * 可以使用@EnableDiscoveryClient注解（通用）
 */
@EnableEurekaClient
@EnableTransactionManagement
@SpringBootApplication
public class MicroserviceUserProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceUserProviderApplication.class, args);
    }
}
