package cn.edu.neusoft.consumer.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author YooLin1c
 * @version 1.0.0
 * @since 2018-03-21 13:51
 */
@Data
public class User {
    private Long id;
    private String username;
    private String name;
    private Integer age;
    private BigDecimal balance;
}
